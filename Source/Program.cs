﻿using System;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;

namespace DIDNT
{
	class Program
	{
		static void Main(string[] args)
		{
			foreach (var arg in args)
			{
				Console.WriteLine(arg);
				switch (arg)
				{
					case "-l":
					case "--list":
						ListInterfaces();
						break;
					case "-s":
					case "--server":
						StartServer();
						break;
					case "--help":
					case "-h":
						ShowHelp();
						break;
					default:
						break;
				}
			}
			if (args.Length == 0)
			{
				StartClient();
			}
			else
			{
				
			}
		}

		private static void StartServer()
		{
			int interfaceID = GetInterfaceID();
			Console.WriteLine(interfaceID);
			var networking = new Networking(interfaceID);
			networking.StartServer();
		}

		private static void StartClient()
		{
			Console.WriteLine("Provide server address.");
			string serverAddress = Console.ReadLine();
			var networking = new Networking(serverAddress);
			networking.StartClient();
		}

		private static void StartAutoServer()
		{
			int interfaceID = GetInterfaceID();
			Console.WriteLine(interfaceID);
			var networking = new Networking(interfaceID);
			networking.StartMulticastServer();
		}

		private static void StartAutoClient()
		{
			var networking = new Networking();
			networking.StartMulticastClient();
		}

		private static void Start()
		{
			string code = @"var i = 5+3;
			return Convert.ToString(i);";
			var result = CodeExecutor.Execute(code);
			Console.WriteLine(result);
		}

		private static int GetInterfaceID()
		{
			Console.WriteLine("Provide interface ID.");
			ListInterfaces();
			Console.Write("Interface ID: ");
			return 0;
			return Convert.ToInt32(Console.ReadLine());
		}

		private static void ListInterfaces()
		{
			Console.WriteLine("ID, Name, IP address");
			var interfaces = NetworkInterface.GetAllNetworkInterfaces();
			int i = 0;
			foreach (var iface in interfaces)
			{
				if (iface.GetIPProperties().UnicastAddresses.Count == 0) { i++; continue; }
				Console.WriteLine($"{i}, {iface.Id}, {iface.GetIPProperties().UnicastAddresses[0].Address}");
				i++;
				
			}
		}

		private static void ShowHelp()
		{
			string help =
@"DIDNT
Usage:
	-h, --help		Show help
	-s, --server	Run server
	(no arguments)	Run client
	-l, --list		List Interfaces
	<iface>			Interface ID";
			Console.WriteLine(help);
		}
	}
}

