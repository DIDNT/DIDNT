﻿using System;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
namespace DIDNT
{
	public class CodeExecutor
	{
		public CodeExecutor()
		{
		}

		private static async Task<string> ExecuteAsync(string code)
		{
			var script = CSharpScript.Create<string>(code);
			var result = await script.RunAsync();
			return result.ReturnValue;
		}

		public static string Execute(string code)
		{
			string[] imports = { "System" };
			var scriptOptions = ScriptOptions.Default.AddImports(imports);
			var result = CSharpScript.EvaluateAsync<string>(code, scriptOptions)
			                         .Result;
			return result;
		}
	}
}
