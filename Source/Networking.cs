using System;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace DIDNT
{
	public class Networking
	{
		int interfaceID;
		string serverAddress;
		TcpClient tcpClient;
		SslStream sslStream;
		X509Certificate certificate;
		string code;

		public Networking(int interfaceID)
		{
			this.interfaceID = interfaceID;
		}
		public Networking(string serverAddress)
		{
			this.serverAddress = serverAddress;
		}
		public Networking()
		{

		}

		public static bool ValidateServerCertificate(
			object sender,
			X509Certificate certificate,
			X509Chain chain,
			SslPolicyErrors sslPolicyErrors)
		{
			return true;
		}

		public void StartMulticastServer()
		{
			UdpClient udpClient = new UdpClient();
			IPAddress address = IPAddress.Parse("224.0.0.200");
			IPEndPoint endPoint = new IPEndPoint(address, 5000);
			var interfaces = NetworkInterface.GetAllNetworkInterfaces();
			var interfaceIP = interfaces[interfaceID]
							  .GetIPProperties().UnicastAddresses[0].Address;
			string ping = "DIDNT Hello;" + interfaceIP;
			Byte[] pingDgram = Encoding.ASCII.GetBytes(ping);
			while (true)
			{
				udpClient.Send(pingDgram, pingDgram.Length, endPoint);
				Byte[] received = udpClient.Receive(ref endPoint);
				string receivedString = Encoding.ASCII.GetString(received);
				if (receivedString == "DIDNT Ack")
				{
					break;
				}
			}
		}

		public void StartServer()
		{

			code = File.ReadAllText("code.txt");

			IPAddress address = IPAddress.Any;
			IPEndPoint endPoint = new IPEndPoint(address, 2000);
			certificate = new X509Certificate2("cert.pfx");

			TcpListener tcpServer = new TcpListener(endPoint);
			tcpServer.Start();
			TcpClient client = tcpServer.AcceptTcpClient();
			sslStream = new SslStream(
			client.GetStream(), false);
			Console.WriteLine("Server started.");
			sslStream.AuthenticateAsServer(certificate);
			Console.WriteLine("SSL connection established.");

			//sslStream.ReadTimeout = 5000;
			//sslStream.WriteTimeout = 5000;

			string message = ReadMessage();
			Console.WriteLine(message);

			if (message == "DIDNT Hello")
			{
				Console.WriteLine($"Received handshake: {message}");
				string result = SendCode(code);
				Console.WriteLine($"Result: {result}");
			}
			else throw new Exception($"Bad handshake: {message}");

		}


		private string SendCode(string code)
		{
			SendMessage(code);
			string result = ReadMessage();
			return result;
		}

		public void StartMulticastClient()
		{
			IPAddress address = IPAddress.Parse("224.0.0.200");
			IPEndPoint endPoint = new IPEndPoint(address, 5000);
			UdpClient udpClient = new UdpClient(endPoint);
			udpClient.Connect(endPoint);
			udpClient.JoinMulticastGroup(address);

			Byte[] received = udpClient.Receive(ref endPoint);
			string receivedString = Encoding.ASCII.GetString(received);
			var splittedReceived = receivedString.ToString().Split(";");
			Console.WriteLine(receivedString.ToString());
			serverAddress = splittedReceived[1];

			string ack = "DIDNT Ack";
			Byte[] ackDgram = Encoding.ASCII.GetBytes(ack);
			udpClient.Send(ackDgram, ackDgram.Length, endPoint);
		}

		public void StartClient()
		{
			IPAddress address = IPAddress.Parse(serverAddress);
			IPEndPoint endPoint = new IPEndPoint(address, 2000);
			try
			{
				tcpClient = new TcpClient();
				try
				{
					tcpClient.Connect(endPoint);					
				}
				catch(Exception ex)
				{
					Console.WriteLine("Connection Error, press enter and try again");
					Console.ReadLine();
				}
				Console.WriteLine("Client connected.");
				sslStream = new SslStream(
					tcpClient.GetStream(),
					false,
					new RemoteCertificateValidationCallback(ValidateServerCertificate),
					null
				);
				sslStream.AuthenticateAsClient("DIDNT");
				Console.WriteLine("SSL connection established.");
				SendMessage("DIDNT Hello");
				Console.WriteLine("Handshake sent.");
				code = ReadMessage();
				string result = CodeExecutor.Execute(code);
				Console.WriteLine($"Result: {result}");
				SendMessage(result);
			}
			catch (Exception ex)
			{
				switch (ex.GetType().ToString())
				{
					case "Microsoft.CodeAnalysis.Scripting.CompilationErrorException":
						Console.WriteLine("Received script compilation errror.");
						break;
				}
			}

		}
		private void ConnectToServer(IPEndPoint endPoint)
		{

		}

		private string ReadMessage()
		{
			byte[] buffer = new byte[2048];
			StringBuilder messageData = new StringBuilder();
			int bytes = -1;
			do
			{
				bytes = sslStream.Read(buffer, 0, buffer.Length);

				// Use Decoder class to convert from bytes to UTF8
				// in case a character spans two buffers.
				Decoder decoder = Encoding.UTF8.GetDecoder();
				char[] chars = new char[decoder.GetCharCount(buffer, 0, bytes)];
				decoder.GetChars(buffer, 0, bytes, chars, 0);
				messageData.Append(chars);
				// Check for EOF.
				if (messageData.ToString().IndexOf("<EOF>") != -1)
				{
					break;
				}
			} while (bytes != 0);

			return messageData.ToString().Replace("<EOF>", "");
		}

		private void SendMessage(string message)
		{
			message = message + "<EOF>";
			Byte[] messageDgram = Encoding.UTF8.GetBytes(message);
			sslStream.Write(messageDgram);
		}

	}
}
